package com.example.spring_thymeleaf.controller;

import com.example.spring_thymeleaf.dto.CreateTodoDTO;
import com.example.spring_thymeleaf.dto.TodoResponseDTO;
import com.example.spring_thymeleaf.entities.Todo;
import com.example.spring_thymeleaf.service.TodoService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/todo")
@CrossOrigin(origins = {"http://localhost:3000"}, methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST})
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponseDTO> findAll(
            @RequestParam(required = false, defaultValue = "", name = "tcon") String contains
    ){
        return todoService.findAll(contains)
                .stream()
                .map(Todo::toResponeDTO)
                .toList();
    }

    @GetMapping("/{id}")
    public TodoResponseDTO findById(@PathVariable("id") int id){
        return todoService.findById(id).toResponeDTO();
    }

    @PostMapping
    public TodoResponseDTO addTodo(@RequestBody CreateTodoDTO createTodoDTO){
        return todoService
                .addTodo(
                        createTodoDTO.title(),
                        createTodoDTO.description(),
                        createTodoDTO.userId()
                )
                .toResponeDTO();
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id){
        todoService.deleteById(id);
    }

}
